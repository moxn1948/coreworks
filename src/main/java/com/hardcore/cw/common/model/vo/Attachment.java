package com.hardcore.cw.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Attachment {
	private int fileNo;
	private String originName;
	private String changeName;
	private String filePath;
	private int type;
	private String status;
	private int versionNo;
	private int easNo;
	private int chatNo;
	private int empDivNo;
	private int bno;
	private int mailNo;
	private String companyName;
	private String signYn;
}
