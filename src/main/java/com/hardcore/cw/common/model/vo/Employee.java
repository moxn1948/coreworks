package com.hardcore.cw.common.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Employee {
	private int seqDivNo;
	private int empNo;
	private String empId;
	private String empPwd;
	private String empName;
	private String extPhone;
	private String phone;
	private String birth;
	private String address;
	private String gender;
	private String fax;
	private Date enrollDate;
	private Date entDate;
	private String status;
	private int jobCode;
	private int posCode;
	private int deptCode;
	private String auth;
}
