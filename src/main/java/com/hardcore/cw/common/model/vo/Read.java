package com.hardcore.cw.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Read {
	private int typeNo;
	private int calNo;
	private int empDivNo;
	private int easNo;
	private int bno;
	private int replyNo;
	private int alramNo;
	private int mailNo;
}
