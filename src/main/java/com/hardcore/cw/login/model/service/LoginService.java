package com.hardcore.cw.login.model.service;

import com.hardcore.cw.common.model.vo.Employee;

public interface LoginService {

	Employee loginEmployee(Employee e);
	
}
