package com.hardcore.cw.login.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Employee;

@Repository
public class LoginDaoImpl implements LoginDao {

	@Override
	public String selectEncPassword(SqlSessionTemplate sqlSession, Employee e) {
		return sqlSession.selectOne("Login.selectPwd", e.getEmpId());
	}

	@Override
	public Employee selectEmployee(SqlSessionTemplate sqlSession, Employee e) {
		return sqlSession.selectOne("Login.selectLoginUser", e);
	}

}
