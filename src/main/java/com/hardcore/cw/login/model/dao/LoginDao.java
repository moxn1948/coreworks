package com.hardcore.cw.login.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.Employee;

public interface LoginDao {

	String selectEncPassword(SqlSessionTemplate sqlSession, Employee e);

	Employee selectEmployee(SqlSessionTemplate sqlSession, Employee e);
}
