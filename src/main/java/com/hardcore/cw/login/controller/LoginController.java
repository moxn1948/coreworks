package com.hardcore.cw.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.login.model.service.LoginService;

@Controller
@SessionAttributes("loginUser")
public class LoginController {
	@Autowired
	private LoginService ls;
	
	
	@PostMapping("login.me")
	public String loginCheck(Employee e, Model model) {
		Employee loginUser = ls.loginEmployee(e);
		
		if(loginUser == null) {
			model.addAttribute("msg", "로그인 실패!");
			return "common/errorPage";
		}
		
		model.addAttribute("loginUser", loginUser);
		
		return "redirect:index.jsp";
	}
	
}
