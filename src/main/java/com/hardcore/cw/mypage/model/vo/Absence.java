package com.hardcore.cw.mypage.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Absence {
	private int absNo;
	private Date absSdate;
	private Date absEdate;
	private String absStime;
	private String absEtime;
	private String absReason;
	private String proxyYn;
	private String absCnt;
	private String status;
	private int empDivNo;
	private String proxyName;
}
