package com.hardcore.cw.calendar.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Calendar {
	private int calNo;
	private String calTitle;
	private String calCnt;
	private Date calSdate;
	private Date calEdate;
	private String calStime;
	private String calEtime;
	private String calKind;
	private String attendant;
	private int empDivNo;
	private String status;
}
