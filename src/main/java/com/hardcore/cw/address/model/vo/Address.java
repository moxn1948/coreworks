package com.hardcore.cw.address.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Address {
	private int type;
	private String addrgroup;
	private int deptCode;
	private int empDivNo;
}
