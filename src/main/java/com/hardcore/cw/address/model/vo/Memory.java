package com.hardcore.cw.address.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Memory {
	private int memoryNo;
	private Date memoryDate;
	private String memoryName;
	private String sunmoon;
	private int outNo;
}
