package com.hardcore.cw.address.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Outsider {
	private int outNo;
	private String outName;
	private String outJob;
	private String outEmail;
	private String outPhone;
	private String outFax;
	private String outCom;
	private String outAddr;
	private int type;
	private String addrgroup;
	private String outPos;
	private String status;
	private String outDept;
}
