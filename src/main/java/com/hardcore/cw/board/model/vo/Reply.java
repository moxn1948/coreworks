package com.hardcore.cw.board.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Reply {
	private int replyNo;
	private String replyCnt;
	private Date replyDate;
	private String rereply;
	private int bno;
	private int empDivNo;
	private String status;
}
