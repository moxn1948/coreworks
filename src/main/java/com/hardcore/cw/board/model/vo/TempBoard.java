package com.hardcore.cw.board.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TempBoard {
	private int bno;
	private int empDivNo;
	private int tempNo;
	private String tempTitle;
	private String tempCnt;
	private String tempType;
}
