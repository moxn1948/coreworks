package com.hardcore.cw.board.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Board {
	private int bno;
	private String btitle;
	private String bcnt;
	private String bwriter;
	private int hits;
	private Date bdate;
	private String essenYn;
	private String belong;
	private String btype;
	private int empDivNo;
	private String status;
}
