package com.hardcore.cw.cms.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Format {
	private int formatNo;
	private String formatName;
	private Date period;
	private String formatDetail;
	private int pathNo;
	private String periodYn;
	private String status;
	private String formatCnt;
}
