package com.hardcore.cw.cms.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Vacation {
	private int vacNo;
	private String vacName;
	private Date vacEdate;
	private String repeatYn;
	private String enforceYear;
	private Date vacSdate;
	private String usingYn;
	private String status;
}
