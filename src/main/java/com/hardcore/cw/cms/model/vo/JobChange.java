package com.hardcore.cw.cms.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JobChange {
	private Date changeDate;
	private int jobName;
	private int changeJob;
	private int empDivNo;
}
