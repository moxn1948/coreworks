package com.hardcore.cw.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PriFormat {
	private int priFormatNo;
	private String priName;
	private String priCnt;
}
