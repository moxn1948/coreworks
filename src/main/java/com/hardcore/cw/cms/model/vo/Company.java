package com.hardcore.cw.cms.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Company {
	private String companyName;
	private String license;
	private String phone;
	private String fax;
	private String name;
	private String address;
	private Date estDate;
	private String domain;
	private String signYn;
	private String signCode;
}
