package com.hardcore.cw.mail.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Mail {
	private int mailNo;
	private int sender;
	private String receiver;
	private String referer;
	private String hiddenRef;
	private String mailTitle;
	private String mailCnt;
	private String importantYn;
	private String protectYn;
	private String resvYn;
}
