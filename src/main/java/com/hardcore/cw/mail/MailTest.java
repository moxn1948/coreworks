package com.hardcore.cw.mail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.apache.commons.mail.util.MimeMessageParser;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class MailTest {
	public static void main(String[] args) throws IOException {
		String bucketName = "coreworks";
		String accessKey = "";
		String secretKey = "";
		
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.US_WEST_2)
				.build();
		
		ObjectListing objects = s3Client.listObjects(bucketName);
		do {
			for(S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
				String key = objectSummary.getKey();
				S3Object object = s3Client.getObject(bucketName, key);
				InputStream mailFileInputStream = object.getObjectContent();
				
				MimeMessage messgae = getMimeMessageForRawEmailString(mailFileInputStream);
				object.close();
				
				MimeMessageParser parser = new MimeMessageParser(messgae);
				try {
					String from = parser.getFrom();
					String htmlContent = parser.parse().getHtmlContent();
					String plain = parser.parse().getPlainContent();
					List<DataSource> fileList = parser.getAttachmentList();
						if(fileList.get(0).getName() != null) {
							DataSource file = parser.findAttachmentByName(fileList.get(0).getName());
							File f = new File("bapbird2.jpg");
							InputStream in = file.getInputStream();
							OutputStream out = new FileOutputStream(f);
							
							byte[] buf = new byte[1024];
							int len = 0;
							
							while((len = in.read(buf)) > 0) {
								out.write(buf, 0, len);
							}
							
							in.close();
							out.close();
						}
					
					
					System.out.println("From : "+ from);
					System.out.println("Content : " + htmlContent);
					System.out.println("plain : " + plain);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} while (objects.isTruncated());
		
	}
	
	public static MimeMessage getMimeMessageForRawEmailString(InputStream mailFileInputStream) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);
		MimeMessage message = null;
		try {
			message = new MimeMessage(session, mailFileInputStream);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;
	}
}
