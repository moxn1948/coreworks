package com.hardcore.cw.chat.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Chat {
	private int chatNo;
	private Date chatDate;
	private String chatCnt;
	private String checkYn;
	private int roomNo;
}
