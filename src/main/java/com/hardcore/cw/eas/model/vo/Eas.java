package com.hardcore.cw.eas.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Eas {
	private int easNo;
	private String easTitle;
	private String limitYn;
	private String protectYn;
	private String emergencyYn;
	private int formatNo;
	private int empDivNo;
	private Date easSdate;
	private String docNo;
	private String tempYn;
	private Date easPeriod;
	private String easDept;
	private String senderName;
	private String proxyName;
	private int receiverName;
	private String type;
}
