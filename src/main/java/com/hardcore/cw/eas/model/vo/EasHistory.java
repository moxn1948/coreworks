package com.hardcore.cw.eas.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EasHistory {
	private String procState;
	private int easNo;
	private int empDivNo;
	private Date procDate;
	private String procTime;
}
