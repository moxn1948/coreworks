package com.hardcore.cw.eas.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MyEas {
	private int myeasNo;
	private int easName;
	private int formatNo;
	private String myeasName;
	private int empDivNo;
	private String type;
	private int pathRank;
}
