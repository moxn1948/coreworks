package com.hardcore.cw.eas.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DocPath {
	private int pathNo;
	private String pathName;
	private int pathOrder;
}
