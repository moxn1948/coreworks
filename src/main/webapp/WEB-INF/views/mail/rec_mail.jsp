<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/mail_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                    	<div id="mail_h2">
                    		<h2>전체 메일</h2>
                    	</div>
                    	<div class="mail_count">
	                    	안읽은 메일<strong>154</strong>
	                    	<span>/</span>
	                    	전체 메일<strong>234</strong>
                    	</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                    	<div class="mail_search">
                       		<input type="text" name="mail_search" id="mail_search" placeholder="메일검색">
                        	<button class="btn_solid_white">검색</button>
                    	</div>
                    	<div class="mail_btn">
	                    	<span class="mail_allchk">
	                    		<input type="checkbox" id="mail_allchk" />
	                    	</span>
                        	<button class="btn_main">스팸신고</button>
	                        <button class="btn_blue">답장</button>
	                        <button class="btn_pink">삭제</button>
                        	<button class="btn_main">전달</button>
                        	<button class="btn_main">읽음</button>
                        	<button class="btn_main">이동</button>
                        </div>
                      
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="mail_tbl">
                                <colgroup>
                                	<col style="width: 3%;">
                                	<col style="width: 3%;">
                                	<col style="width: 1%;">
                                	<col style="width: 15%;">
                                    <col style="width: *;">
                                    <col style="width: 10%;">
                                </colgroup>
                                <tr>
                                  	<td><input type="checkbox" /></td>
                                  	<td><img src="${contextPath}/resources/images/empty_star.svg" class="mail_star"></td>
                                  	<td><img src="${contextPath}/resources/images/clip.svg" class="mail_clip"></td>
                                  	<td>taco from trello</td>
                                  	<td><a href="#">메일 제목</a></td>
                                  	<td>01-17 15:11</td>
                                </tr>
                                <tr>
                                  	<td><input type="checkbox" /></td>
                                  	<td><img src="${contextPath}/resources/images/star.svg" class="mail_star"></td>
                                  	<td><img src="${contextPath}/resources/images/clip.svg" class="mail_clip"></td>
                                  	<td>taco from trello</td>
                                  	<td><a href="#">메일 제목</a></td>
                                  	<td>01-17 15:11</td>
                                </tr>
                            
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>
<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });

</script>

<jsp:include page="../chat/chat.jsp"/>
</body>
</html>
