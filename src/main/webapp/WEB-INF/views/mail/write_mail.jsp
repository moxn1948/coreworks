<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/mail_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                    	<div id="mail_h2">
                    		<h2>메일 쓰기</h2>
                    	</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                    	<div class="mail_search">
                       		<input type="text" name="mail_search" id="mail_search" placeholder="메일검색"> <!-- 다우랑 네이버 둘다 있길래 일단 넣었음 -->
                        	<button class="btn_solid_white">검색</button>
                    	</div>
                    	<div class="mail_btn">
                        	<button class="btn_blue">보내기</button>
                        	<button class="btn_main">미리보기</button>
                        	<button class="btn_main">임시저장</button>
                        </div>
                      
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="mail_tbl">
                                <colgroup>
                                	<col style="width: 10%;">
                                    <col style="width: *;">
                                </colgroup>
                                <tr>
                                	<th>
                                		받는 사람
                                		<input type="checkbox" id="to_me" name="to_me" /><label for="to_me">나에게</label>
                                	</th>
                                  	<td>
                                  		<input type="text" id="mail_reciver" name="mail_reciver" />
                                  		<button class="btn_main">주소록</button>
                               		</td>
                                </tr>
                                <tr>
                                	<th>
                                		참조
                                		+
                                	</th>
                                  	<td>
                                  		<input type="text" id="mail_ref" name="mail_ref" />
                                  		<button class="btn_main">주소록</button>
                               		</td>
                                </tr>
                                <tr>
                                  	<td>숨은 참조</td>
                                  	<td>
                                  		<input type="text" id="mail_hidden_ref" name="mail_hidden_ref" />
                                  		<button class="btn_main">주소록</button>
                                  	</td>
                                </tr>
                                <tr>
                                	<th>
                                		제목
                                		<input type="checkbox" name="imp" id="imp" /><label for="imp">중요</label>
                                	</th>
                                  	<td><input type="text" id="mail_tit" name="mail_tit" /></td>
                                </tr>
                                <tr>
                                	<th>
                                		파일첨부
                                		+
                                	</th>
                                  	<td colspan="2">
                                  		<form action="" method="post" enctype="multipart/form-data"></form>
                                  		<input type="hidden" id="" /> <!-- 상단의 작성자 등 내용 여기다 넣고 multipart로 전송함 -->
                                  		<input type="hidden" id="" />
                                  		<input type="hidden" id="" />
                                  		<input type="hidden" id="" />
                                  		<input type="hidden" id="" />
                                  		<input type="hidden" id="" />
                                  		<div class="mail_file">
	                                  		<div class="mail_file_btn">
		                                  		<button class="btn_main" id="select_file_btn">파일 선택</button>
		                                  		<input type="file" id="select_file" hidden />
		                               			<button class="btn_pink">전체 삭제</button>
	                               			</div>
	                               			<div class="mail_file_size">
	                               				<p class="file_size">
	                               					<span>
														<span>0Byte</span> <!-- 색 넣을거면 클래스 넣고 바꿀 것 -->
														/
														<strong>50MB</strong>
													</span>
	                               				</p>
	                               			</div>
                               			</div>
                               			<div class="file_zone"> <!-- 파일 첨부 영역 -->
                               				<div class="area_txt">
                               					<span>파일 첨부 영역</span>
                               				</div>
	                               			<div class="file_list_zone">
	                               				<ul class="file_attach_list"> <!-- 파일 첨부시 여기다 목록 보여주기 -->
	                               					<li><!-- 대용량 파일 첨부 ? 가능 ? -->
	                               						<input type="checkbox" /> <!-- display:none; -->
	                               						<span class="file">
	                               							<span class="file_del_btn"></span> <!-- onclick으로 클릭시 삭제 -->
	                               							<span class="file_name">bapbird.jpg</span> <!-- 파일명 -->
	                               						</span>
	                               						<span class="fileinfo">
	                               							<span class="size"></span>
	                               						</span>
	                               					</li>
	                               				</ul>
	                               			</div>
                               			</div>
                               		</td>
                                </tr>
                            	<tr>
                            		<td colspan="2">
                            			텍스트 에 디 터
                            		</td>
                            	</tr>
                            	<tr>
                            		<td colspan="2">
                            			<button class="btn_main">예약메일</button> <!-- 날짜 , 시간 선택할 수 있게끔 -->
                            			<button class="btn_main">보안메일</button> <!-- 메일 열때 암호 필요한 것 -->
                            		</td>
                            	</tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });

</script>

<jsp:include page="../chat/chat.jsp"/>
</body>
</html>
