<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/first_menu.jsp" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">	
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사원등록</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                    <div class="main_cnt_list clearfix">
						
					<div id="logoImgArea">
						<img id="titleImg" width="120px" height="120px">
						<a href="#" class="button btn_main com_img">사진 등록</a>
					</div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">비밀번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">지급</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="">-<input type="text" name="" id="">-<input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="datepicker1"></div>
                        </div>
                        
                        
                        
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""><a href="#" class="button btn_pink com_addr" onclick="DaumPostcode()">주소 검색</a></div>           
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_desc com_addr2"><input type="text" class="addr" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_desc com_addr2"><input type="text"  class="addr" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit">성별</div>
                        <div class="box_radio">
						<input type="radio" name="gender" id="M" value="M">
						<label for="M">남</label> 
						<input type="radio" name="gender" id="F" value="F">
						<label for="F">여</label>
					</div>
					</div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>	
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>	
                            <div class="main_cnt_desc"><input type="text" name="" id="datepicker2"></div>
                        </div>
                        
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>
                <a href="#" class="button btn_blue save" onclick="ok()">확인</a>
			<a href="#" class="button btn_blue add" onclick="cancel()">취소</a>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
	        /* // 주 메뉴 분홍색 하이라이트 처리
	        $("#nav .nav_list").eq(7).addClass("on"); */

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(4).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(4).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(4).find(".sub_menu_list").eq(1).addClass("on");
        
         $("#datepicker1").datepicker({
             changeMonth: true,
             changeYear: true,
             nextText: '다음 달',
             prevText: '이전 달',
             dateFormat: "yy/mm/dd",
             showMonthAfterYear: true , 
             dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
             monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
         });
         
         $("#datepicker2").datepicker({
             changeMonth: true,
             changeYear: true,
             nextText: '다음 달',
             prevText: '이전 달',
             dateFormat: "yy/mm/dd",
             showMonthAfterYear: true , 
             dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
             monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
         });
    });
    
 // 주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>