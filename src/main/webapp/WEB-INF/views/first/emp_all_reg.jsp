<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/first_menu.jsp" />
    
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사용자 등록</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       <h3>사용자 일괄 등록</h3>
                       <br>
                       	<div class="main_cnt_list clearfix list2">		
                       	<div class="regdesc">
					<label>신규 등록할 사용자 정보를 엑셀파일(CSV)로 업로드 하여, 최대 100건까지 일괄 등록할 수
						있습니다. 등록 양식 샘플을 다운로드 받아, 신규 구성원 정보를 등록하세요. 샘플 다운로드 양식에 맞지 않거나,
						유효하지 않은 정보는 빨간 글씨로 표시됩니다. 이 경우 상단 메뉴에서 선택 또는 리스트에서 직접 수정한 후 등록하세요.
						관리자가 설정한 비밀번호는 임시비밀번호이며, 사용자가 직접 비밀번호를 변경한 후 이용할 수 있습니다.</label>
				</div>		
							</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                     <div class="menu_tit"><h2>파일 선택</h2></div>
                      <div class="main_cnt_desc allreg"><input type="text" class="allreg2"name="" id="">
                       <a href="" class="button btn_blue find">찾기</a>
                      </div>
                      
                      
	            <div class="annbtn">
	            <a href="" class="button btn_blue add">적용</a>
	            </div>
                    
                    <!-- 기본 테이블 끝 -->
			<div class="tbl_common tbl_basic form">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn align ">
                                <tr>
                                    <td class="tit"><input type="checkbox" name="check1" id="check1" value="1"></td>
                                    <td class="tit align">이름</td>
                                    <td class="tit align">이메일</td>
                                    <td class="tit align">입사일</td>
                                    <td class="tit align">부서</td>
                                    <td class="tit align">직급</td>
                                    <td class="tit align">직책</td>
                			   </tr>
                               <tr>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		
                               </tr>

                            </table>
                        </div>
                    </div>
                 	             </div>  
                 	             
                 	             
                  </div>
                  </main>  
            </div>
           
            
            
 
<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
    	
    	
        // 주 메뉴 분홍색 하이라이트 처리
       // $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(4).addClass("on");
        
        // 열리는 메뉴
       // $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(1).addClass("on");
        
        $(".find").click(function() {
        	$("tbl_common tbl_basic form").show();
        	
        });
    });
    
    
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>