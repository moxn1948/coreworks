<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/first_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
               <div>
			<a href="#" class="button btn_main next" onclick="next()">다음 단계</a>
               </div>
                
                
                    <div class="menu_tit empreg">
                    <h2>사원등록</h2>
                    <a href="#" class="button btn_pink skip">건너뛰기</a>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        
                    </div>
                <div class="regbtn">	
			<a href="#" class="button btn_blue reg" onclick="">개별등록</a>
			<a href="#" class="button btn_blue reg" onclick="">일괄등록</a>
				</div>	
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <tr class="tbl_main_tit">
                                    <th>사번</th>
                                    <th>이름</th>
                                    <th>ID</th>
                                    <th>부서</th>
                                    <th>직급</th>
                                    <th>직책</th>
                                </tr>
				

				</table>
                    </div>
                     
                          
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
		
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
var i = 1;
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        //$("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(4).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
      	
        });
    
    
    function save() {
    	confirm("저장하시겠습니까?")
    }
    
    function next() {
    	confirm("다음 단계로 진행 시 이전 단계로 돌아갈 수 없습니다. 진행하시겠습니까? (※ 추후 관리자 메뉴에서 수정이 가능합니다.)")
    }
    
    
    
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>