<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/notice_menu.jsp" />


<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit tit_rel"><h2>전체 알림</h2><button class="btn_pink tit_abs" id="alldel">전체 삭제</button></div>
                    
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: *;">
                                    <col style="width: 5%;">
                                </colgroup>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
                                    		<i class="far fa-comment-dots"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<span class="notice_type">[댓글 등록]</span> <span class="notice_content">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
	                                    	<i class="far fa-clipboard"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<span class="notice_type readcheck">[댓글 등록]</span> <span class="notice_content readcheck">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix readcheck">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
	                                    	<i class="far fa-envelope"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<p>asdfasdf</p>
                                    		<p>asdfasdf</p>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
	                                    	<i class="far fa-calendar-check"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<p>asdfasdf</p>
                                    		<p>asdfasdf</p>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>
<script type="text/javascript"> 
	$(document).ready(function () {
		$('#scroll_area').jscroll({
			autoTrigger: true, 
			loadingHtml: '<div class="next"><img src="${ contextPath }/resources/images/loading.gif" alt="Loading" /></div>', 
			nextSelector: 'a.nextPage:last'
		}); 
	}); 	
</script>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        //$("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });
    
    $("#alldel").click(function() {
    	var con = confirm("알림을 전체 삭제하시겠습니까?");
    });
</script>

<jsp:include page="../chat/chat.jsp"/>
</body>
</html>
