<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<link href="${ contextPath }/resources/css/style_hr.css" rel="stylesheet">
<style>
.modal{max-width: 700px;}
</style>
<h3 class="main_tit">서식별 결재선 지정</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="">
            <option value="">사원명</option>
            <option value="">부서명</option>
        </select>
        <input type="text" name="" id="">
        <button class="btn_solid_pink">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn eas_addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="newEasPop2" class="tree_menu">
                    <ul>
                        <li class="folder">기안
                            <ul>
                                <li class="folder">공통서식
                                    <li class="folder">가족사서식
                                        <ul>                                          
                                            <li>업무기안</li>
                                            <li>업무협조</li>
                                            <li>신규 가맹점 승인</li>
                                            <li>지급/승인한도 관련</li>
                                            <li>가맹점 결재수</li>
                                            <li>가맹점 정산주</li>
                                        </ul>
                                    </li>
                                </li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부</li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn eas_addr_info_pop_ctn">
            <div class="addr_info_pop eas_addr_info_pop">
                <div class="tit_wrap clearfix">
                    <div class="name_area">
                        <p class="name_cnt eas_name_cnt">상세 정보</p>
                    </div>                   
                </div>
                <div class="desc_wrap clearfix">
                    
                    <div class="info_area">
                        <ul class="info_ctn">
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">제목</p>
                                <p class="cnt">업무기안</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">전사 문서함</p>
                                <p class="cnt">일반>업무기안</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">보존 연한</p>
                                <p class="cnt">5년</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">기안 부서</p>
                                <p class="cnt">
                                	<select>
										<option>gg</option>
										<option>hhh</option>                                	
                                	</select>
                                </p>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>
<script>
    
    $(function(){
        $("#newEasPop2").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                /* if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                }  */
            },
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");


    });
</script>