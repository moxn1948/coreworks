<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    /* 모달 크기 변경 */
    .modal{max-width: 800px;}
    /* 모달 안에 데이트 피커 넣을 때 */
    .ui-datepicker{z-index: 6000 !important;}
</style>
<h3 class="main_tit">새 일정 등록</h3>
<div class="calc_pop calc_new_sch">
    <div class="calc_ctn_list clearfix">
        <div class="tit">종류</div>
        <div class="cnt">
            <select name="" id="calc_type">
                <option value="type1">개인일정</option>
                <option value="type2">부서일정</option>
                <option value="type3">단체일정</option>
                <option value="type4">회사일정</option>
            </select>
        </div>
    </div>
    <div class="calc_ctn_list clearfix calc_day">
        <div class="tit">기간</div>
        <div class="cnt sel_day">
            <input type="text" id="from" name="from" class="date" placeholder="시작일">
            <select name="" id="start_time" class="date_time date_time_1">
                <option value="" hidden>시간선택</option>
            </select>
            <input type="text" id="to" name="to" class="date" placeholder="종료일">
            <select name="" id="end_time" class="date_time date_time_2">
                <option value="" hidden>시간선택</option>
            </select>
        </div>
        <div class="cnt all_day">
            <input type="text" id="allDay" name="" class="date" placeholder="날짜">
        </div>
        <div class="cnt day_chk">
            <input type="checkbox" name="" id="allDayChk"><label for="allDayChk">종일</label>
        </div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">제목</div>
        <div class="cnt"><input type="text" name="" id="" placeholder="제목을 작성해주세요." class="calc_tit"></div>
    </div>
    <!-- <div class="calc_ctn_list clearfix">
        <div class="tit">설명</div>
        <div class="cnt"><p class="txt">dd</p></div>
    </div> -->
    <div class="calc_ctn_list clearfix">
        <div class="tit">설명</div>
        <div class="cnt"><textarea name="" id="" placeholder="설명을 입력해주세요." rows="6"></textarea></div>
    </div>
    <!-- <div class="calc_ctn_list clearfix calc_type_ctn calc_type_ctn_1">
        <div class="tit">개인</div>
        <div class="cnt"><input type="text" name="" id=""></div>
    </div> -->
    <div class="calc_ctn_list clearfix calc_type_ctn calc_type_ctn_2">
        <div class="tit">공람자</div>
        <div class="cnt">
            <jsp:include page="../pop/calc_new_person.jsp"/>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_white"><a href="#" rel="modal:close">취소</a></button>
    <button class="btn_main">등록</button>
</div>

<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function(){
        $(".calc_type_ctn_1").css({display : "block"});  

        $("#calc_type").on("change", function(){
            var type = $(this).val();
            if(type == "type1"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_1").css({display : "block"});    
            }else if(type == "type2"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_2").css({display : "block"});
            }else if(type == "type3"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_2").css({display : "block"});
            }else if(type == "type4"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_2").css({display : "block"});
            }
        });
        $("#allDay").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        from = $( "#from" )
                .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat : "yy/mm/dd",
                nextText: '다음 달',
                prevText: '이전 달',
                dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
                to = $( "#to" ).datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat : "yy/mm/dd",
                    nextText: '다음 달',
                    prevText: '이전 달',
                    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });
    
        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( "yy/mm/dd", element.value );
            } catch( error ) {
                date = null;
            }
        
            return date;
        }
        setTimeSel("#start_time", 23);
        setTimeSel("#end_time", 23);
        function setTimeSel(sel, num){
            for(var i = 0; i <= num; i++){
                
                var hour;
                var day;
                if(i > 12){
                   day = i - 12;
                }else{
                    day = i;
                }

                if(day < 10){
                    if(i < 12){

                        hour = "0" + day + " : 00 AM";
                    }else{

                        hour = "0" + day + " : 00 PM";
                    }
                }else{
                    if(i < 12){
                        hour = day + " : 00 AM";
                    }else{
                        hour = day + " : 00 PM";
                    }
                }

                $(sel).append("<option value='" + hour + "'>" + hour + "</option>");
                
                if(day < 10){
                    if(i < 12){
                        hour = "0" + day + " : 30 AM";
                    }else{
                        hour = "0" + day + " : 30 PM";
                    }
                }else{
                    if(i < 12){
                        hour = day + " : 30 AM";
                    }else{
                        hour = day + " : 30 PM";
                    }
                }

                $(sel).append("<option value='" + hour + "'>" + hour + "</option>");
            }
        }

        $("#allDayChk").on("change",function(){
            if($(this).prop("checked") == true){
                $(".sel_day").css({display : "none"});
                $(".all_day").css({display : "block"});
            }else{
                $(".sel_day").css({display : "block"});
                $(".all_day").css({display : "none"});
            }
        });

        $(document).on("click", "#personList tr td", function(){
            $(this).parent("tr").remove();
        });
        $("#personList .tbl_common table tr td").on("mouseover",function(){
            $(this).parents("tr").css({
                backgroundColor : "#efefef"
            });
        }).on("mouseleave", function(){
            $(this).parents("tr").css({
                backgroundColor : "transparent"
            });
        });

        $(this).on("click",".calc_pop .fancytree-title", function(){
            if(!$(this).parent().hasClass("fancytree-folder")){


                var node = $(this)[0].innerHTML;
                $("#personList").append("<tr><td>" +
                                            node
                                            + "</td><td>" +
                                            node
                                            + "</td><td>" +
                                            node
                                            + "</td><td>" +
                                            node
                                            + "</td></tr>");
            }
        });

    });

</script>