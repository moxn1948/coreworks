<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!-- tree api 관련 시작 : tree api 없는 페이지면 주석 해제 -->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<!-- tree api 관련 끝 -->
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">

<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="">
            <option value="">사원명</option>
            <option value="">부서명</option>
        </select>
        <input type="text" name="" id="">
        <button class="btn_solid_main">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="calcNewPerson" class="tree_menu">
                    <ul>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li data-cstrender="true">김진호 회장<input type="hidden" value="100"></li>
                                            <li data-cstrender="true">김진호 회장<input type="hidden" value="100"></li>
                                        </ul>
                                    </li>
                                </li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부</li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn">
            <div class="tbl_common tbl_basic">
                <p class="tbl_tit">명단</p>
                <div class="tbl_wrap">
                    <table class="tbl_ctn" id="personList">
                        <colgroup>
                            <col style="width: 15%;">
                            <col style="width: 30%;">
                            <col style="width: 15%;">
                            <col style="width: 40%;">
                        </colgroup>
                        <tr>
                            <th>사원명</th>
                            <th>직책/직급</th>
                            <th>부서</th>
                            <th>이메일</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>  
    </div>
</div>
<script>
    
    $(function(){
        $("#calcNewPerson").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;

            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");


    });
</script>