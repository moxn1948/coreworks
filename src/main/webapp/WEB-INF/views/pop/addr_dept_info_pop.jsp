<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">주소록 상세보기</h3>
<div class="addr_info_pop_wrap">
    <div class="addr_info_pop addr_dept_info_pop">
        <div class="tit_wrap clearfix">
            <div class="name_area">
                <p class="name_cnt"></p>
            </div>
            <div class="link_area">
                <button class="btn_solid_main">이메일</button>
                <button class="btn_solid_main">1:1대화</button>
            </div>
        </div>
        <div class="desc_wrap clearfix">
            <div class="img_area">
                 <!-- 사용자 이미지 없을때 -->
                <div class="user_img_wrap user_img_none">
                    <i class="fas fa-user-circle"></i>
                </div>
                <!-- 사용자 이미지 있을때 -->
                <!-- <div class="user_img_wrap user_img_has">
                    <img src="${contextPath}/resources/images/user_img.jpg" alt="">
                </div> -->
            </div>
            <div class="info_area">
                <ul class="info_ctn">
                    <li class="info_list clearfix">
                        <p class="tit">이름</p>
                        <p class="cnt">최원준</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">직책/직급</p>
                        <p class="cnt">매니저/대리</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">부서</p>
                        <p class="cnt">개발부</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">이메일</p>
                        <p class="cnt">bap@coreworks.info</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">내선번호</p>
                        <p class="cnt">02131324</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">핸드폰번호</p>
                        <p class="cnt">010-1231-1412</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">팩스</p>
                        <p class="cnt">02124231</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>