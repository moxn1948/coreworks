<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    /* 모달 크기 변경 */
    .modal{max-width: 500px;}
</style>
<h3 class="main_tit">새 일정 등록</h3>
<div class="calc_pop calc_sch_det">
    <div class="calc_ctn_list clearfix">
        <div class="tit">종류</div>
        <div class="cnt"><p class="txt">부서일정</p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">날짜</div>
        <div class="cnt"><p class="txt">20/12/12 04: 00 PM - 20/12/13 04: 00 PM</p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">제목</div>
        <div class="cnt"><p class="txt">아 졸라 피곤해요</p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">설명</div>
        <div class="cnt"><p class="txt">부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정부서일정</p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">공람자</div>
        <div class="cnt">
            <div class="tbl_common tbl_basic">
                <div class="tbl_wrap">
                    <table class="tbl_ctn">
                        <colgroup>
                            <col style="width: 15%;">
                            <col style="width: 30%;">
                            <col style="width: 15%;">
                            <col style="width: 40%;">
                        </colgroup>
                        <tr>
                            <th>사원명</th>
                            <th>직책/직급</th>
                            <th>부서</th>
                            <th>이메일</th>
                        </tr>
                        <tr>
                            <td>사원명</td>
                            <td>직책/직급</td>
                            <td>부서</td>
                            <td>이메일</td>
                        </tr>
                        <tr>
                            <td>사원명</td>
                            <td>직책/직급</td>
                            <td>부서</td>
                            <td>이메일</td>
                        </tr>
                        <tr>
                            <td>사원명</td>
                            <td>직책/직급</td>
                            <td>부서</td>
                            <td>이메일</td>
                        </tr>
                        <tr>
                            <td>사원명</td>
                            <td>직책/직급</td>
                            <td>부서</td>
                            <td>이메일</td>
                        </tr>
                        <tr>
                            <td>사원명</td>
                            <td>직책/직급</td>
                            <td>부서</td>
                            <td>이메일</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_white"><a href="#" rel="modal:close">취소</a></button>
    <button class="btn_main">등록</button>
</div>

<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

</script>