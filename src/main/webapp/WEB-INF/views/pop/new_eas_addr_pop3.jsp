<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!-- tree api 관련 시작 : tree api 없는 페이지면 주석 해제 -->
<!-- <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script> -->
<!-- tree api 관련 끝 -->
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<link href="${ contextPath }/resources/css/style_hr.css" rel="stylesheet">
<style>
.modal{max-width: 840px;}
</style>
<h3 class="main_tit">공람자 설정</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="">
            <option value="">사원명</option>
            <option value="">부서명</option>
        </select>
        <input type="text" name="" id="">
        <button class="btn_solid_main">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="newEasAddrPop3" class="tree_menu">
                    <ul>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li data-cstrender="true">김진호 회장</li>
                                            <li data-cstrender="true">김진호 회장</li>
                                        </ul>
                                    </li>
                                </li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부</li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn eas_line_select_div">
            <div class="addr_info_pop">
                <div class="tit_wrap clearfix">
                </div>
                <div class="desc_wrap clearfix top0">
                        <table class="eas_line_select_div_tbl">
                        	<colgroup>
                        		<col style="width:8%;">
                        		<col style="width:12%;">
                        		<col style="width:16%;">
                        		<col style="width:20%;">
                        		<col style="width:10%;">
                        		<col style="width:20%;">
                        		<col style="width:14%;">
                        	</colgroup>
                        	<tr>
                        		<th>순번</th>
                        		<th>부서</th>
                        		<th>직급/직책</th>
                        		<th>처리방법</th>
                        		<th>처리자</th>
                        		<th>이메일</th>
                        		<th>순서변경</th>
                        	</tr>
                        	<tr>
                        		<td>1</td>
                        		<td>구매파트</td>
                        		<td>매니저/팀원</td>
                        		<td>
                        			<select>
                        				<option>확인</option>
                        				<option>합의</option>
                        				<option>결재</option>
                        			</select>
                        		</td>
                        		<td>조소연</td>
                        		<td>jsy1004@core.com</td>
                        		<td>
                        			<button class="new_eas_addr_pop_btn3">▲</button>
                        			<button class="new_eas_addr_pop_btn3">▼</button>
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>1</td>
                        		<td>구매파트</td>
                        		<td>매니저/팀원</td>
                        		<td>
                        			<select>
                        				<option>확인</option>
                        				<option>합의</option>
                        				<option>결재</option>
                        			</select>
                        		</td>
                        		<td>조소연</td>
                        		<td>jsy1004@core.com</td>
                        		<td>
                        			<button class="new_eas_addr_pop_btn3">▲</button>
                        			<button class="new_eas_addr_pop_btn3">▼</button>
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>1</td>
                        		<td>구매파트</td>
                        		<td>매니저/팀원</td>
                        		<td>
                        			<select>
                        				<option>확인</option>
                        				<option>합의</option>
                        				<option>결재</option>
                        			</select>
                        		</td>
                        		<td>조소연</td>
                        		<td>jsy1004@core.com</td>
                        		<td>
                        			<button class="new_eas_addr_pop_btn3">▲</button>
                        			<button class="new_eas_addr_pop_btn3">▼</button>
                        		</td>
                        	</tr>
                        </table>
                   
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>
<script>
    
    $(function(){
        $("#newEasAddrPop3").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                /* if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                }  */
            },
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");


    });
    
 	 //결재선 순서변경 스크립트
    $(document).on('click', '.new_eas_addr_pop_btn3', function(){

    	if($(this).attr('id')=='upBtn'){
    		console.log('upBtn 클릭됨');
	
    		var $tr = $(this).parent().parent(); // 클릭한 버튼이 속한 tr 요소
    		$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
    		
    		$tr.children().eq(0).text('순서!!');
    		
    	}else {
    		console.log('downBtn 클릭됨');
    		
    		var $tr = $(this).parent().parent(); // 클릭한 버튼이 속한 tr 요소
    		$tr.next().after($tr); // 현재 tr 의 다음 tr 뒤에 선택한 tr 넣기
    		
    		$tr.children().eq(0).text('순서!!');
    	}
    	
    	
    	
    });
</script>