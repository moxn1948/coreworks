<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">주소록 상세보기</h3>
<div class="addr_info_pop_wrap">
    <div class="addr_info_pop addr_com_info_pop">
        <div class="tit_wrap clearfix">
            <div class="name_area">
                <p class="name_cnt"></p>
            </div>
            <div class="link_area">
                <button class="btn_pink">삭제</button>
                <button class="btn_main">수정</button>
            </div>
        </div>
        <div class="desc_wrap clearfix">
            <div class="info_area">
                <ul class="info_ctn">
                    <li class="info_list clearfix">
                        <p class="tit">이름</p>
                        <p class="cnt">최원준</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">회사명</p>
                        <p class="cnt">오렌지레드</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">직책/직급</p>
                        <p class="cnt">매니저/대리</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">부서</p>
                        <p class="cnt">개발부</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">번호</p>
                        <p class="cnt">02131324</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">이메일</p>
                        <p class="cnt">bap@coreworks.info</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">팩스</p>
                        <p class="cnt">02124231</p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">회사주소</p>
                        <p class="cnt">19211 서울시 강남구 역삼동 테헤란로 12번길 C강의장</p>
                    </li>
                    <li class="info_list clearfix">
                        <div class="spe_day_tbl_wrap">
                            <table class="spe_day_tbl">
                                <colgroup>
                                    <col style="width: 40%;">
                                    <col style="width: 30%;">
                                    <col style="width: 30%;">
                                </colgroup>
                                <tr>
                                    <th>기념일명</th>
                                    <th>날짜</th>
                                    <th>양/음력</th>
                                </tr>
                                <tr>
                                    <td>생일</td>
                                    <td>96/12/31</td>
                                    <td>양력</td>
                                </tr>
                                <tr>
                                    <td>생일</td>
                                    <td>96/12/31</td>
                                    <td>양력</td>
                                </tr>
                                <tr>
                                    <td>생일</td>
                                    <td>96/12/31</td>
                                    <td>양력</td>
                                </tr>
                                <tr>
                                    <td>생일</td>
                                    <td>96/12/31</td>
                                    <td>양력</td>
                                </tr>
                            </table>

                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>