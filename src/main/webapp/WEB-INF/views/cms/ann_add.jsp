<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/cms_menu.jsp" />
    
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>연차 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       <h3>연차 생성 조건</h3>
                       	<div class="main_cnt_list clearfix list2">
								<ul class="list">
									<li>연차는 [연차관리 - 기본 설정] 페이지에서 설정한 연차 생성 조건에 따라 생성됩니다.</li>
									<li> 그룹웨어 구매 후 근태/휴가를 처음 사용하거나 수동 설정이 필요한 경우, 아래 확인 사항을 확인하시고 [저장] 버튼을 클릭하여 연차를 수동 생성하세요.</li>
								</ul>
								
	                       			<div class="descr">
	                       				 <label>※ 연차는 휴가 생성 조건에서 설정한 생성 일자에 매년 1회 자동 생성되므로, 특별한 경우를 제외하고는 수동 생성할 필요가 없습니다.</label>
	                       			</div>		
							</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                
                                
                                <tr>
                                    <td class="tit1">생성년도</td>
                                    <td class="tit tbselect">
                                    <select name="" id="">
                           		    <option value="">2020</option>
                            		<option value="">2021</option>
                       				</select>
                       		 		</td>                           
                                </tr>
                                <tr>
                                    <td class="tit2_qwe"><a href="#">휴가일수</a></td>
                                    <td class="tbdesc" >
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								N/N+1
                							</div>
                							<div>
                								16
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								N+2
                							</div>
                							<div>
                								17
                							</div>              											
                						</div>
                                    		<div class="tbdiv">
                							<div class="tbdiv2">
                								N+3
                							</div>
                							<div>
                								18
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								N+4
                							</div>
                							<div>
                								19
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								N+5
                							</div>
                							<div>
                								20
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								N+6
                							</div>
                							<div>
                								21
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								N+7
                							</div>
                							<div>
                								22
                							</div>              											
                						</div>				
	                                    <div class="tbdiv">
	                							<div class="tbdiv2">
	                								N+8
	                							</div>
	                							<div>
	                								23
	                							</div>              											
	                						</div>                              	
                               		 </tr>
                            
                                <tr >
                                    <td class="tit1 tit3"rowspan="3"><a href="#">확인사항</a></td>
                                    <td class="tit tbauto">아래 내용을 확인 후 체크해주세요.</td>                 
                                </tr>
                                <tr class="tbbot">
                                	<td class="tit tbauto"><input type="checkbox" name="check1" id="check1" value="1"><label for="check1">위 조건으로 전 직원의 연차 휴가를 생성(재설정)합니다.<br>
생성 조건이 회사 규정에 맞지 않으면 [휴가관리 - 기본 설정] 탭에서 설정을 변경한 후 이용하세요.</label></td>                   
                                </tr>
                                <tr class="tbbot">
                                	<td class="tit tbauto"><input type="checkbox" name="check2" id="check2" value="2"><label for="check2">조직에 속하지 않거나 입사일이 입력되지 않은 사용자는 휴가가 생성되지 않습니다.<br>
휴가 대상자를 먼저 확인하시기 바랍니다.</label></td>                   
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                 	               
            <div class="annbtn">
            <a href="" class="button btn_blue add">저장</a>
            </div>
            
          
            
            <div class="menu_tit"><h2>포상휴가 생성</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       <h3>연차 생성 조건</h3>
                       	<div class="main_cnt_list clearfix list2">
								<ul class="list">
									<li>근무 기간에 따른 연차 휴가 외 별도 포상 휴가를 부여할 때 사용하는 기능입니다.</li>
									<li> ‘생성 후' 입력란에 최종 포상 휴가 일수를 입력합니다. <br>
									예) 현재 포상 휴가를 3일 받은 직원에게 2일을 추가하고 싶다면, ‘생성 후' 입력란에 5일을 입력합니다.</li>
									<li> 포상 휴가 일수는 ‘정수' 또는 ‘0.5’ 단위로 입력할 수 있습니다.</li>
								</ul>	
							</div>
			<div>
			<div class="annbtn2">
            <a href="#ann_add_pop" rel="modal:open" class="button btn_blue">추가</a>
            </div>
             <div class="main_cnt_desc2"><a href="" class="button btn_blue add test">확인</a><input type="text" name="" id=""></div>
			</div>
							
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic ann_add">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn align">
                                <tr>
                                    <td class="tit"><input type="checkbox" name="check1" id="check1" value="1"></td>
                                    <td class="tit align">이름</td>
                                    <td class="tit align">ID</td>
                                    <td class="tit align">부서</td>
                                    <td class="tit align">연차</td>
                                    <td class="tit align">추가 연차</td>
                                    <td class="tit align">추가 일수</td>
                			   </tr>
                               <tr>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		
                               </tr>

                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                 	               
            <div class="annbtn">
            <a href="" class="button btn_pink add left">삭제</a>
            <a href="" class="button btn_blue add">저장</a>
            </div>
            
            
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->

      
            </div><!-- inner_rt end -->
 
<!-- popup include -->
<div id="ann_add_pop" class="modal">
	<jsp:include page="../pop/addr_pop_1.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(1).addClass("on");
    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>