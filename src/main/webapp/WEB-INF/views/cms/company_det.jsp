<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">		
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>회사정보 입력	</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
						
					<div id="logoImgArea">
						<img id="titleImg" width="120px" height="120px">
						
					</div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_tit">어렌쥐 퀌퍼니</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사업자번호</div>
                            <div class="main_cnt_tit">113789654</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">전화번호</div>
                            <div class="main_cnt_tit">010-2555-2555</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                           <div class="main_cnt_tit">010-2333-2333</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">대표자명</div>
                            <div class="main_cnt_tit">김진호</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                             <div class="main_cnt_tit">10917</div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit addr com_addr2">주우우우우우소</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit addr com_addr2">주우우우우우소</div>
                        </div>
                        
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">도메인</div>
                            <div class="main_cnt_tit">.coreworks.com</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">설립일</div>
                            <div class="main_cnt_tit">SYSDATE</div>
                        </div>
                        
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>
        <div class="annbtn">
            <a href="" class="button btn_blue companyup">수정</a>
            </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
	             // 주 메뉴 분홍색 하이라이트 처리
	        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(5).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>