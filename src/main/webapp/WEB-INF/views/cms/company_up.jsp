<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">	
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>회사정보 입력	</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
						
					<div id="logoImgArea">
						<img id="titleImg" width="120px" height="120px">
						<a href="#" class="button btn_main com_Img">사진 등록</a>
					</div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사업자번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">전화번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">대표자명</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""><a href="#" class="button btn_pink com_addr">주소 검색</a></div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_desc com_addr2"><input type="text" class="addr1" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_desc com_addr2"><input type="text" class="addr2" name="" id=""></div>
                        </div>
                        
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">설립일</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                         <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">도메인</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                         <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">서명사용여부</div>
                            <div class="main_cnt_desc">
                            <input type="radio" name="sign" id="use"><label for="use">사용</label>
                            <input type="radio" name="sign" id="nuse"><label for="nuse">사용 안함</label>
                            
                            
                            </div>
                        </div>
                        
                <a href="#" class="button btn_blue com_sav">저장</a>
                <a href="#" class="button btn_pink com_can">취소</a>
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
	        // 주 메뉴 분홍색 하이라이트 처리
	        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(5).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(4).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(4).find(".sub_menu_list").eq(1).addClass("on");
    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>