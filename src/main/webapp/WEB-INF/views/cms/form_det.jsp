<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/cms_menu.jsp" />
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">


<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
<div class="main_ctn">
		<div class="menu_tit">
			<h2>기안서식 관리</h2>
		</div>
		<!-- 탭 영역 위 컨텐츠 시작 -->
		<div class="main_cnt">
                            <div class="main_cnt_tit">폴더</div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_desc">공통</div>
                        </div>
                        <div class="main_cnt_list clearfix2">                      	
                            <div class="main_cnt_tit">양식명</div>
                            <div class="main_cnt_desc">휴가신청서</div>
                        	
                        	<div class="tit">
                            <div class="main_cnt_tit">보존연한</div>
                            <div class="main_cnt_desc">1년</div>
                            </div> 	 
                        </div>
                       
                       		<div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">설명</div>
                            <div class="main_cnt_desc">설명 입력란</div>	    
                       		</div>
		<!-- 탭 영역 위 컨텐츠 끝 -->	
	</div>
	<!-- 탭 영역 시작 -->
	<div class="main_ctn">
    	
		<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap eas_tbl_doc">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 80px;">
                                    <col style="width: *;">
                                    <col style="width: 80px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                </colgroup>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
                                    <td rowspan="2" class="doc_con">20200115-몰라-몰라</td>
                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>
                                    <td class="doc_con gray doc_tit">팀장</td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                               	<tr class="eas_tit">                                 
                                    <td rowspan="3" class="doc_con">이원경</td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                </tr>                             
                                <tr class="eas_tit" >
                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
                                    <td rowspan="2" class="doc_con">20/01/15</td>                         	                                                                   
                                </tr>
                                <tr class="eas_tit"></tr>                                
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
                                    <td rowspan="2" class="doc_con">구매파트</td>
                                    <td rowspan="4" class="doc_con small doc_tit">합의</td>
                                    <td class="doc_con gray doc_tit">합의자</td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="3" class="doc_con border_bottom">합의자 이름</td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>                                
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
                                    <td rowspan="2" class="doc_con">조문정</td>                              
                                </tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">수신자</td>
                                    <td colspan="7" class="doc_con">양승현</td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">제목</td>
                                    <td colspan="7" class="doc_con">뱁새 사료 구매의 건</td>                                
                                </tr>
                                <!-- 기안 본문 작성 부분 -->
                                <tr class="eas_tit">
                                    <td rowspan="10" colspan="8" class="eas_content"><div class="fr-view formdet"></div></td>                              
                                </tr>
                               	<tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <!-- 기안 본문 작성 부분 끝 -->                                                       
                            </table>
                            
                        </div>
                    </div>
        <!-- 기본 테이블 끝 -->
		</div>
		<!-- 본문 탭 끝 -->
		<!-- 에디터 -->
						
    <!-- 탭 영역 끝 -->
    </div>
	<!-- 메인 컨텐츠 영역 끝! -->

<!-- inner_rt end -->
</div>
</main>
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        var editor = new FroalaEditor('.fr-view')
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(1).addClass("on");
        
        var path = '${ contextPath }';
	
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");

       
        
        // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
        //$("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        //$("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0));

    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>