<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
				
                    <div class="menu_tit"><h2>휴무 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       <h3>연차 생성 조건</h3>
                       	<div class="main_cnt_list clearfix list2">
                       			
								<ul class="list">
									<li>휴무관리에서는 회사의 고정적인 휴무를 지정할 수 있습니다.</li>
									<li>휴무관리에서 설정된 일자는 캘린더에 자동 연동됩니다.</li>
									<li>휴무로 지정된 일자는 삭제가 불가능하오니, 신중하게 등록해주시기 바랍니다.</li>
								</ul>
							
							</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                              
                                	
                               
                                <tr class="tbl_main_tit holitr1">
                                    <th rowspan="3">휴무일자</th>                                
                                    <td class="tbl_main_tit holitd1">
                                    <div class="main_cnt_list clearfix holiinput">
                                    <div class="settext">휴무명 </div>
                                    <div class="main_cnt_holiday"><input type="text" name="" id=""></div>
                                    <div class="usecheck">
                            		<div class="main_cnt_useyn"><input type="checkbox" name="" id=""></div>
                                    <div class="settext">사용여부</div>                                
                                    </div>
                                    
                            			</div>
                            			</td>
                                </tr>
                                <tr class="tbl_main_tit holitr2">
                                	<td class="tbl_main_tit holitd2" >
                                    <div class="main_cnt_list clearfix holiinput">
                                    	<div class="settext">휴무일시</div>                                
                            			<div class="main_cnt_desc"><input type="text" name="" id="datepicker1"></div>
                            			<div class="main_cnt_desc intext"> ~ </div>
                            			<div class="main_cnt_desc"><input type="text" name="" id="datepicker2"></div>
                       				 </div>
 	
                        			</td>              
                                </tr>
                                
                                <tr class="tbl_main_tit holitr3">
                                	<td>
                                	<div class="tbl_main_tit holitd3">
                       				 	<div class="holiinput1">
                       				 	 <input type="radio" name="htype" id="repeat" value="repeat">
										 <label for="repeat">매 해 반복</label>
                       				 	
										 
										 
                       				 	 <input type="radio" name="htype" id="target" value="target">
										 <label for="target">지정 년도만</label>
										  <select name="" id="" class="holiselect">
                            <option value="">2020</option>
                            <option value="">2021</option>
                       		 </select> 
										 </div>
							
                           
                       
                       				 </div></td>
                                	
                                </tr>
                              
                                
                                
                                
				

				</table>
                    </div>
                     
                          
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                  <a href="#" class="button btn_blue holiadd">저장</a>	 
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
		
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
var i = 1;
var j = 1;
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on");
        
        // 열리는 메뉴
       // $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
       // $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        
        
        $("#datepicker1").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
		
        $("#datepicker2").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
    });

    
    
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>