<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/submenu.jsp" />
<!-- tree api 관련 시작 -->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<!-- tree api 관련 끝 -->
			
			 <div id="scroll_area" class="inner_rt">
              <!-- 메인 컨텐츠 영역 시작! -->
                  <div class="tbl_top_wrap">
                      <div class="menu_tit"><h2>주소록 공통요소</h2></div>
                      <!-- 테이블 위 컨텐츠 시작 -->

                        <!-- 부서 셀렉트 박스 시작 -->
                        <div class="addr_sel_wrap">
                            <div class="add_sel">
                                <input type="text" name="" id="" placeholder="부서를 선택하세요." readonly class="add_sel_ipt">
                                <div class="add_sel_opt">
                                    <div id="addrSelTree" class="tree_menu">
                                        <ul>
                                            <li class="folder">회장실
                                                <ul>
                                                    <li class="folder">홍보부
                                                        <li class="folder">회장실
                                                            <ul>
                                                                <li class="folder">홍보부</li>
                                                            </ul>
                                                        </li>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="folder">총무부
                                                <ul>
                                                    <li class="folder">인사부
                                                        <ul>
                                                            <li class="folder">홍보부</li>
                                                            <li class="folder">홍보부</li>
                                                        </ul>
                                                    </li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                            <li class="folder">회장실
                                                <ul>
                                                    <li class="folder">홍보부</li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                            <li class="folder">총무부
                                                <ul>
                                                    <li class="folder">인사부
                                                        <ul>
                                                            <li class="folder">홍보부</li>
                                                            <li class="folder">홍보부</li>
                                                        </ul>
                                                    </li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 부서 셀렉트 박스 끝 -->
                      <!-- 테이블 위 컨텐츠 끝 -->
                  </div>
                  <!-- 전자결재 테이블 시작 -->
                  <div class="tbl_common eas_wrap">
                      <div class="tbl_wrap">
                          <table class="tbl_ctn">
                              <tr>
                                  <th>제목1</th>
                                  <th>제목2</th>
                                  <th>제목3</th>
                                  <th>제목4</th>
                                  <th>제목5</th>
                              </tr>
                              <tr>
                                  <td><a href="#addrPop1" rel="modal:open">주소록 예시 팝업</a></td>
                                  <td><a href="#">내용2</a></td>
                                  <td><a href="#">내용3</a></td>
                                  <td><a href="#">내용4</a></td>
                                  <td><a href="#">내용5</a></td>
                              </tr>
                              <tr>
                                  <td><a href="#">내용1</a></td>
                                  <td><a href="#">내용2</a></td>
                                  <td><a href="#">내용3</a></td>
                                  <td><a href="#">내용4</a></td>
                                  <td><a href="#">내용5</a></td>
                              </tr>
                          </table>
                      </div>
                  </div>
                  <!-- 전자결재 테이블 끝 -->
                  <!-- 페이저 시작 -->
                  <div class="pager_wrap">
                      <ul class="pager_cnt clearfix">
                      <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                      <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">2</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                      <li class="pager_com pager_num on"><a href="javascrpt: void(0);">4</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">5</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                      <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                      <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                      </ul>
                  </div>
                  <!-- 페이저 끝 -->
              <!-- 메인 컨텐츠 영역 끝! -->
              </div><!-- inner_rt end -->


<div id="addrPop1" class="modal">
    <jsp:include page="../pop/addr_pop_1.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");

        // 부서 셀렉트 박스
        $("#addrSelTree").fancytree({
            imagePath: "skin-custom/",
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");

        $(".add_sel_ipt").on("click", function(){
            $(this).siblings(".add_sel_opt").slideToggle();
        });


    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>