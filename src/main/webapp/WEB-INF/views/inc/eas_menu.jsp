<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <!-- <button class="btn_solid">새 결재 진행</button> -->
                    <td><a href="#newEas" rel="modal:open" class="button btn_solid">새 결재 진행</a></td>
                </div>
                <div id="menu_area" class="tree_shape">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list add">
                            <a href="#">내 결재</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list">
                                    <a href="#">결재대기</a>
                                </li>                              
                                <li class="sub_menu_list">
                                	<a href="#">결재진행</a>
                                </li>
                                <li class="sub_menu_list">
                                	<a href="#">결재반려</a>
                                </li>
                                <li class="sub_menu_list">
                                	<a href="#">임시보관함</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu_list add">
                            <a href="#">참여 결재</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">결재대기</a></li>
                                <li class="sub_menu_list"><a href="#">결재진행</a></li>
                                <li class="sub_menu_list"><a href="#">결재반려</a></li>
                            </ul>
                        </li>
                        <li class="menu_list add">
                            <a href="#">공람</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">공람대기</a></li>
                                <li class="sub_menu_list"><a href="#">공람완료</a></li>
                                <li class="sub_menu_list"><a href="#">보낸공람</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- <button class="btn_white eas_line_select">서식별 결재선 지정</button> -->
        </div><!-- inner_lt end -->
        
        <div class="eas_menu_line_select_div">
            	<div class="div2"><a href="#myEasLine" rel="modal:open" class="eas_menu_line_select"><i class="fas fa-cog"></i>서식별 결재선 지정</a></div>
        </div>
        
<!-- 새 결재 진행 popup include -->
<div id="newEas" class="modal">
	<jsp:include page="../pop/new_eas_pop.jsp" />
</div>
<!-- 서식별 결재선 지정 popup include -->
<div id="myEasLine" class="modal">
	<jsp:include page="../pop/new_eas_pop2.jsp" />
</div>
