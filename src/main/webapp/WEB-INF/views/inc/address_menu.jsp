<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />

<!-- tree api 관련 시작 -->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<!-- tree api 관련 끝 -->

<script type="text/javascript">
    $(function(){
        $("#deptTree").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $("#teamTree").fancytree({
            imagePath: "skin-custom/",
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $("#myTree").fancytree({
            imagePath: "skin-custom/",
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });

        $(".fancytree-container").addClass("fancytree-connectors");
        
    });
  </script>

<main id="container" class="address_page"> 
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <button class="btn_solid">연락처 추가</button>
                </div>
                <div class="tree_menu_wrap">
                    <div id="deptTree" class="tree_menu">
                        <ul>
                            <li class="folder" id="deptTit">사내주소록<input type="hidden" value="으악!">
                                <ul>
                                    <li class="folder">회장실
                                        <ul>
                                            <!-- <li data-cstrender="true">김진호 회장</li> -->
                                            <li class="folder">홍보부
                                                <li class="folder">회장실
                                                    <ul>
                                                        <li class="folder">홍보부</li>
                                                    </ul>
                                                </li>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="folder">총무부
                                        <ul>
                                            <li class="folder">인사부
                                                <ul>
                                                    <li class="folder">홍보부</li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                    <li class="folder">총무부
                                        <ul>
                                            <li class="folder">인사부
                                                <ul>
                                                    <li class="folder">홍보부</li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div id="teamTree" class="tree_menu">
                        <ul>
                            <li class="folder" id="teamTit">공용주소록
                                <ul>
                                    <li class="folder">공통</li>
                                    <li class="folder">총무부
                                        <ul>
                                            <li class="folder">인사부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div id="myTree" class="tree_menu">
                        <ul>
                            <li class="folder" id="myTit">내주소록
                                <ul>
                                    <li class="folder">공통</li>
                                    <li class="folder">총무부
                                        <ul>
                                            <li class="folder">인사부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li class="folder">홍보부</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- inner_lt end -->
