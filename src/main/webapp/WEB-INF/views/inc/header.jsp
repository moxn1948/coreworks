<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>COREWORKS</title>
<!-- modal cdn -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
<!-- 공통 css --> 
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/common.css">
<!-- jquery cdn -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<!-- font awesome cdn -->
<script src="https://kit.fontawesome.com/ccbb1cc624.js" crossorigin="anonymous"></script>
<!-- jQuery Modal cdn -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="icon" type="image/png"  href="./favicon.ico"/>
</head>
<body>
    <div class="wrap">
        <header id="header">
            <div class="full_ct">
                <div class="inner_lt">
                    <h1 class="logo_wrap">
                        <a href=""><img src="${ contextPath }/resources/images/logo_2.png" alt="corework 로고" class="logo"></a>
                    </h1>
                </div><!-- inner_lt end -->
                <div class="inner_rt">
                    <div class="nav_wrap">
                        <nav id="nav">
                            <ul class="nav_ctn clearfix">
                                <li class="nav_list"><a href="#">메일</a></li>
                                <li class="nav_list"><a href="#">전자결재</a></li>
                                <li class="nav_list"><a href="#">문서함</a></li>
                                <li class="nav_list"><a href="#">주소록</a></li>
                                <li class="nav_list"><a href="#">일정</a></li>
                                <li class="nav_list"><a href="#">게시판</a></li>
                                <li class="nav_list"><a href="#">사원관리</a></li>
                                <li class="nav_list"><a href="#">회사관리</a></li>
                            </ul>
                        </nav>
                    </div><!-- nav_wrap end -->
                    <div class="gnb_wrap">
                        <div class="gnb_ctn notice_wrap">
                            <!-- 알림 -->
                            <a href="#" class="notice_link">
                                <i class="far fa-bell"></i>
                            </a>
                        </div>
                        <div class="gnb_ctn logout_wrap">
                            <!-- 로그아웃 -->
                            <a href="#" class="logout_link">
                                <i class="fas fa-power-off"></i>
                            </a></div>
                        <div class="gnb_ctn user_wrap"><a href="#" class="clearfix">
                            <div class="info_wrap">
                                <p class="info_ctn">최원준 대리</p>
                                <p class="info_ctn">개발부서(매니저)</p>
                            </div>
                            <!-- 사용자 이미지 없을때 -->
                            <!-- <div class="user_img_wrap user_img_none">
                                <i class="fas fa-user-circle"></i>
                            </div> -->
                            <!-- 사용자 이미지 있을때 -->
                            <div class="user_img_wrap user_img_has">
                                <img src="${contextPath}/resources/images/user_img.jpg" alt="">
                            </div>
                        </a></div>
                    </div><!-- user_wrap end -->
                </div><!-- inner_rt end -->
            </div>
        </header>