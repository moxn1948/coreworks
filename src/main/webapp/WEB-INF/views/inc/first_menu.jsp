<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/header.jsp" />    
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    
                </div>
                <div id="menu_area">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="#">1. 회사정보 입력</a></li>
                        <li class="menu_list"><a href="#">2. 부서구조 설정</a></li>
                        <li class="menu_list"><a href="#">3. 직급 관리</a></li>
                        <li class="menu_list"><a href="#">4. 직책 관리</a>
                        <li class="menu_list"><a href="#">5. 사원 등록</a>
                        <li class="menu_list"><a href="#">6. 운영자계정 등록</a>
                        <li class="menu_list"><a href="#">7. 전자결재 서식 등록</a>
                        	<ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">서식관리</a></li>
                                <li class="sub_menu_list"><a href="#">폴더관리</a></li>
                             </ul>
                        </li>
                        
                       
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->