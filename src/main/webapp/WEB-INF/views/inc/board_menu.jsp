<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:include page="./header.jsp" />
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <button class="btn_solid">글쓰기</button>
                </div>
                <div id="menu_area">
                   <ul class="menu_ctn clearfix head_menu" >
                      <li class="menu_list must_read_list"><a href="#" style="font-size: 14px;"><i class="far fa-check-circle"></i><br>필독</a></li>
                        <li class="menu_list my_board"><a href="#" style="font-size: 14px;">MY<br>내가 쓴 글</a></li>
                   </ul>
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="#">전체 공지사항</a></li>
                        <li class="menu_list"><a href="#">팀 공지사항</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->