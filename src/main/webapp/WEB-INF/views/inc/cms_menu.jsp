<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/header.jsp" />    
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
          
                </div>
                <div id="menu_area">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="#">휴가관리</a>
                        	<ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">휴가설정</a></li>
                                <li class="sub_menu_list"><a href="#">휴가추가</a></li>
                             </ul>
                        </li>
                        	
                        <li class="menu_list"><a href="#">휴무관리</a></li>
                        
                        <li class="menu_list"><a href="#">기안서식 관리</a>
                       		 <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">서식관리</a></li>
                                <li class="sub_menu_list"><a href="#">폴더관리</a></li>
                             </ul>
                        </li>
                        
                        <li class="menu_list"><a href="#">부서 관리</a></li>
                        
                        <li class="menu_list"><a href="#">직급/직책 관리</a>
                       		 <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">직급관리</a></li>
                                <li class="sub_menu_list"><a href="#">직책관리</a></li>
                             </ul>
                        </li>
                        
                        <li class="menu_list"><a href="#">회사정보 관리</a></li>
                        
                       
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->