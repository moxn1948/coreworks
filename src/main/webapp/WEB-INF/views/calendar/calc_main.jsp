<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/calendar_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
			<div id="scroll_area" class="inner_rt">
				<!-- 메인 컨텐츠 영역 시작! -->
				<div class="main_ctn">
					<!-- ** 코드 작성부 시작 -->
					<div class="main_cnt">
						dd
						<a href="#calcDeptSch" rel="modal:open">일정상세 팝업</a>
					</div>
					<!-- ** 코드 작성부 끝 -->
				</div>
				<!-- 메인 컨텐츠 영역 끝! -->
			</div><!-- inner_rt end -->
		</div>
	</main>
</div>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	$(function () {
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(4).addClass("on");

		// 캘린더 api
        $("#main_menu_cal").jqxCalendar({width: 218, height: 220});
	});
</script>        
<div id="calcDeptSch" class="modal">
    <jsp:include page="../pop/calc_sch_det_pop.jsp" />
</div>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>