<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 개인정보 설정-정보확인  페이지
--%>

<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>개인정보 설정</h2></div>

                    <div class="main_info">
	                    <div class="img_area">
	                    	<img src="${ contextPath }/resources/images/user_img.jpg" class="main_img">
	                    </div>
                    	
                    	<table class="emp_info">
                    		<tr>
                                <td>이름</td>
                                <td>최원준</td>
                            </tr>
                            <tr>
                                <td>아이디</td>
                                <td>bapbird99@kh.com</td>
                            </tr>
                            <tr>
                                <td>사번</td>
                                <td>9999</td>
                            </tr>
                            <tr>
                                <td>부서</td>
                                <td>개발부서</td>
                            </tr>
                            <tr>
                                <td>직책/직급</td>
                                <td>팀장/대리</td>
                            </tr>
                            <tr>
                                <td>내선전화</td>
                                <td>02-123-4567</td>
                            </tr>
                            <tr>
                                <td>휴대전화</td>
                                <td>010-1033-9999</td>
                            </tr>
                            <tr>
                                <td>생일</td>
                                <td>96/09/09</td>
                            </tr>
                            <tr>
                                <td>주소</td>
                                <td>경기도 파주시 구구동 구구마을 구구아파트 9동 9호</td>
                            </tr>
                            <tr>
                                <td>성별</td>
                                <td>남</td>
                            </tr>
                            <tr>
                                <td>팩스번호</td>
                                <td>02-987-6543</td>
                            </tr>
                            <tr>
                                <td>입사일</td>
                                <td>17/03/21</td>
                            </tr>
                        </table>
                    </div>
                        <div id="btnSet">
                            <button class="btn_main">수정하기</button>
                        </div>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>
<jsp:include page="../chat/chat.jsp"/>

<!-- 결재이력상세보기 팝업 -->
<div id="popListAbsence" class="modal">
	<jsp:include page="mypage_pop_list_abs.jsp" />
</div>

<!-- 대결자 상세 팝업 -->
<div id="popDetailAbsence" class="modal">
	<jsp:include page="mypage_pop_detail_abs.jsp" />
</div>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');
    });
</script>
</body>
</html>
