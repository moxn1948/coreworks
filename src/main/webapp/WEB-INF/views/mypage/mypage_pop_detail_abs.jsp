<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">부재 상세보기</h3>

                    <div class="tbl_common tbl_basic absence_detail">
                    	<div class="tbl_wrap">
                            <table class="tbl_ctn">
	                                <tr>
										<th>시작일</th>
										<th>종료일</th>
										<th>사유</th>
										<th>대리결재자</th>
	                                </tr>
	                                <tr>
	                                	<td>20/01/03 09:00 AM</td>
	                                	<td>20/01/04 01:00 PM</td>
	                                	<td>특별휴가</td>
	                                	<td>이원경</td>
	                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <h4 class="main_tit">참고사항</h4>
                    <div class="ann_reason">참고사항 보여지는 곳!<br>1<br>1<br>1<br>1<br>1<br>1<br>1<br>1<br>1<br>1<br>1<br>1<br>1</div>
                    
                    <div class="close_area">
                    	<button class="btn_main detail_close"><a href="#" rel="modal:close">확인</a></button>
                    </div>