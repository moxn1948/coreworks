<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 개인정보 설정-정보확인  페이지
--%>
<jsp:include page="../inc/myPage_menu.jsp" />
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>개인정보 설정</h2></div>

	                <form>
                    <div class="main_info">
	                    	<div class="img_area">
	                    		<img src="${ contextPath }/resources/images/user_img.jpg" class="main_img">
								<div class="file_input_div">
									<button class="btn_white file_input_button">등록</button>
									<input type="file" class="file_input_hidden" onchange="javascript: document.getElementById('fileName').value = this.value" />
								</div>
	                    	</div>
	                    	
	                    	<table class="emp_info">
	                    		<tr>
	                                <td>이름</td>
	                                <td>최원준</td>
	                            </tr>
	                            <tr>
	                                <td>아이디</td>
	                                <td>bapbird99@kh.com</td>
	                            </tr>
	                            <tr>
	                                <td>사번</td>
	                                <td>9999</td>
	                            </tr>
	                            <tr>
	                                <td>부서</td>
	                                <td>개발부서</td>
	                            </tr>
	                            <tr>
	                                <td>직책/직급</td>
	                                <td>팀장/대리</td>
	                            </tr>
	                            <tr>
	                                <td>내선전화</td>
	                                <td><input type="text" name="ext_tel"></td>
	                            </tr>
	                            <tr>
	                                <td>휴대전화</td>
	                                <td>
	                                	<input type="text" size="2" class="call_phone">-
	                                	<input type="text" size="3" class="call_phone">-
	                                	<input type="text" size="3" class="call_phone">
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>생일</td>
	                                <td><input type="date"></td>
	                            </tr>
	                            <tr>
	                                <td>주소</td>
	                                <td>
	                                	<div class="address_form_wrap">
		                                    <div class="">
		                                    	<input type="text" name="" id="postcode_form" placeholder="우편번호" readonly>
		                                    		<a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a>
		                                    	</div>
		                                    <div class=""><input type="text" name="" id="address_form" placeholder="주소" readonly></div>
		                                    <div class=""><input type="text" name="" id="detailAddress_form" placeholder="상세주소"></div>
	                               		</div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>성별</td>
	                                <td>
	                                	<div class="presence">
				                        	<input type="radio" name="gender" value="M" id="male">
				                            <label for="male">남</label>
				                            <input type="radio" name="gender" value="F" id="female">
				                            <label for="female">여</label>
			                            </div>
	                               	</td>
	                            </tr>
	                            <tr>
	                                <td>팩스번호</td>
	                                <td><input type="text"></td>
	                            </tr>
	                            <tr>
	                                <td>입사일</td>
	                                <td>17/03/21</td>
	                            </tr>
	                        </table>
	                    </div>
	                   
                        <div id="btnSet">
                            <button type="button" class="btn_main">취소</button>
                            <button class="btn_main">확인</button>
                        </div>
                	 </form>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>
<jsp:include page="../chat/chat.jsp"/>

<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');

        $("#postcode_form, #address_form").on("click", function(){
            $("#address_srch_btn").trigger("click");
        });
        // 주소 검색 끝
    });
    
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }
</script>
</body>
</html>
