<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!-- tree api 관련 시작 : tree api 없는 페이지면 주석 해제 -->
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<!-- tree api 관련 끝 -->
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<style>
.modal{max-width: 620px;}
</style>
<h3 class="main_tit">주소록 상세보기</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="">
            <option value="">사원명</option>
            <option value="">부서명</option>
        </select>
        <input type="text" name="" id="">
        <button class="btn_solid_main">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="addrDefaltPop" class="tree_menu">
                    <ul>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부
                                    <li class="folder">회장실
                                        <ul>
                                            <li class="folder">홍보부</li>
                                            <li data-cstrender="true">김진호 회장</li>
                                            <li data-cstrender="true">김진호 회장</li>
                                        </ul>
                                    </li>
                                </li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">회장실
                            <ul>
                                <li class="folder">홍보부</li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                        <li class="folder">총무부
                            <ul>
                                <li class="folder">인사부
                                    <ul>
                                        <li class="folder">홍보부</li>
                                        <li class="folder">홍보부</li>
                                    </ul>
                                </li>
                                <li class="folder">홍보부</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn">
            <div class="addr_info_pop ">
                <div class="tit_wrap clearfix">
                    <div class="name_area">
                        <p class="name_cnt"></p>
                    </div>
<!--                     <div class="link_area">
                        <button class="btn_main">이메일</button>
                        <button class="btn_main">1:1대화</button>
                    </div> -->
                </div>
                <div class="desc_wrap clearfix in_abs_add">
                    <div class="img_area">
                        <!-- 사용자 이미지 없을때 -->
                        <div class="user_img_wrap user_img_none">
                            <i class="fas fa-user-circle"></i>
                        </div>
                        <!-- 사용자 이미지 있을때 -->
                        <!-- <div class="user_img_wrap user_img_has">
                            <img src="${contextPath}/resources/images/user_img.jpg" alt="">
                        </div> -->
                    </div>
                    <div class="info_area">
                        <ul class="info_ctn">
                            <li class="info_list clearfix">
                                <p class="tit">이름</p>
                                <p class="cnt">최원준</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">직책/직급</p>
                                <p class="cnt">매니저/대리</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">부서</p>
                                <p class="cnt">개발부</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">이메일</p>
                                <p class="cnt">bap@coreworks.info</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">내선번호</p>
                                <p class="cnt">02131324</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">핸드폰번호</p>
                                <p class="cnt">010-1231-1412</p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">팩스</p>
                                <p class="cnt">02124231</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>
<script>
    
    $(function(){
        $("#addrDefaltPop").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
/*                     $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    }); */
                } 
            },
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");


    });
</script>