<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/address_menu.jsp" />
    
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                        <h2>주소록</h2>
                        <!-- 공용주소록/내주소록일 때만 노출 -->
                        <button class="btn_blue main_tit_rt_btn">그룹수정</button>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        <div class="main_srch_wrap">
                            <select name="" id="">
                                <option value="">이름</option>
                                <option value="">이메일</option>
                            </select>
                            <input type="text" name="" id="">
                            <button class="btn_solid_main">검색</button>
                        </div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 20%;">
                                    <col style="width: 20%;">
                                    <col style="width: 10%;">
                                    <col style="width: 25%;">
                                    <col style="width: 25%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>이름</th>
                                    <th>직책/직급</th>
                                    <th>부서</th>
                                    <th>이메일</th>
                                    <th>번호</th>
                                </tr>
                                <tr>
                                    <td><a href="#deptAddrPop" rel="modal:open">사내주소록팝업</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">매니저/대리</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">개발부</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">bap@coreworks.info</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">023423432</a></td>
                                </tr>
                                <tr>
                                    <td><a href="#comAddrPop" rel="modal:open">부서/내주소록팝업</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">매니저/대리</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">개발부</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">bap@coreworks.info</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">023423432</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- popup include -->
<div id="deptAddrPop" class="modal">
	<jsp:include page="../pop/addr_dept_info_pop.jsp" />
</div>
<div id="comAddrPop" class="modal">
	<jsp:include page="../pop/addr_com_info_pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(3).addClass("on");

        var tree = $.ui.fancytree.getTree("#deptTree");
        tree.visit(function(node){
            if(node.key == "deptTit"){
                node.setExpanded(true);
            }
        });


        $(".tbl_common table tr td").on("mouseover",function(){
            $(this).parents("tr").css({
                backgroundColor : "#efefef"
            });
        }).on("mouseleave", function(){
            $(this).parents("tr").css({
                backgroundColor : "transparent"
            });
        });
    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>
