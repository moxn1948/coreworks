<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>

            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사원 등록</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
                  
               <div id="logoImgArea">
                  <img id="titleImg" width="120px" height="120px">
                  <a href="#" class="button btn_main" style="margin:10 0">사진 등록</a>
               </div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc emp_reg_pd"><input type="text" name="" id=""></div>
                            <div class="main_cnt_desc"><button class="btn_main">중복확인</button></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc emp_reg_pd"><input type="text" name="" id=""></div>
                            <div class="main_cnt_desc"><button class="btn_main">중복확인</button></div>
                        </div>                       
                        <div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">비밀번호</div>
                           <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                       </div> 
                       <div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">비밀번호 확인</div>
                           <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                       </div> 
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">
	                            <select name="" id="">
	                            	<option value="">부서</option>
	                            	<option value="">부서</option>
	                        	</select>
	                      	</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급</div>
                            <div class="main_cnt_tit">
                            	<select name="" id="">
	                            	<option value="">직급</option>
	                            	<option value="">직급</option>
	                        	</select>
                            </div>
                            <div class="main_cnt_tit">직책</div>
                            <div class="main_cnt_desc">
                            	<select name="" id="">
	                            	<option value="">직책</option>
	                            	<option value="">직책</option>
	                        	</select>
                            </div>
                        </div>                      
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="phone"> - <input type="text" name="" id="phone"> - <input type="text" name="" id="phone"></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="datepicker" class="datepicker"></div>
                        </div>                
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc">
                            	<!-- 주소 검색 폼 시작 -->
                               <div class="address_form_wrap">
                                    <div class=""><input type="text" name="" id="postcode_form" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a></div>
                                  <div class=""><input type="text" name="" id="address_form" placeholder="주소" readonly></div>
                                  <div class=""><input type="text" name="" id="detailAddress_form" placeholder="상세주소"></div>
                               </div>
                                <!-- 주소 검색 폼 끝 -->
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">성별</div>
                            <div class="main_cnt_tit">
                            	<input type="radio" name="" id=""><label>남</label>
                           	</div>
                           	<div class="main_cnt_tit">
                            	<input type="radio" name="" id=""><label>여</label>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="datepicker2" class="datepicker"></div>
                        </div>
                        <div class="insert_btn">
                        	<button class="btn_main">취소</button>
                			<button class="btn_main">확인</button>
                   		</div>                                              
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                    <!-- <div style="float:right;">
                        <button class="btn_pink">퇴사처리</button>
                		<button class="btn_main">수정하기</button>
                    </div> -->              
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
        $("#datepicker2").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
    });
    
 	//주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
</script>
</body>
</html>