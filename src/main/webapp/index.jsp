<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
<c:if test="${ empty loginUser }">
	<jsp:forward page="WEB-INF/views/board/new_board.jsp"/>
</c:if>
<c:if test="${ !empty loginUser }">
	<jsp:forward page="WEB-INF/views/main/main.jsp"/>
</c:if>